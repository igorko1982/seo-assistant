{{--
  Template Name: Главная сраница
--}}
@extends('layouts.app')
{{-- @section('content') @while(have_posts()) @php(the_post())
@include('partials.page-header') @include('partials.content-page') @endwhile @endsection --}}


<section class="intro">
  <div class="container intro__container">
    <div class="slider intro__slider">
      <div class="slider__slides">
        <div class="slider__slide">
          <div class="slider__content">
            <div class="slider__title">Не хватает
              <span>времени</span>
              <br/> заниматься своим сайтом?
            </div>
            <div class="slider__text">доверьте это дело нашим профессионалам.</div>
            <div class="slider__cta">
              <button class="button button--outline button--primary button--large">Заказать</button>
            </div>
          </div>
        </div>
        <div class="slider__slide"></div>
        <div class="slider__slide"></div>
      </div>
      <div class="slider__controls">
        <div class="slider__control slider__control--prev">
          <img class="inject" src="@asset('images/arrow-prew.svg')" height="10" alt="">
        </div>
        <div class="slider__control slider__control--next">
          <img class="inject" src="@asset('images/arrow-next.svg')" height="10" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="about">
  <div class="container about__container">


    <div class="about__title h3">
      <span>Seo Assistant это:</span>
    </div>
    <div class="about__content">
      <div class="about__blocks">
        <div class="about__block">
          <div class="about__block-image">
            <img src="@asset('images/user.svg')" height="75" alt="">
          </div>
          <div class="about__block-name h4">креативная команда профессионалов.</div>
          <div class="about__block-text">подробнее о на подробнее о нас подробнее о насс</div>
        </div>
        <div class="about__block">
          <div class="about__block-image">
            <img src="@asset('images/browser.svg')" height="75" alt="">
          </div>
          <div class="about__block-name h4">надежная техподдержка.
          </div>
          <div class="about__block-text">подробнее о на подробнее о нас подробнее о насс</div>
        </div>
        <div class="about__block">
          <div class="about__block-image">
            <img src="@asset('images/medal.svg')" height="75" alt="">
          </div>
          <div class="about__block-name h4">в ногу с технологиями.</div>
          <div class="about__block-text">подробнее о на подробнее о нас подробнее о насс</div>
        </div>
      </div>
      <div class="about__cta">
        <a href="#" class="button button--outline button--primary ">заказать</a>
      </div>

    </div>
  </div>
</section>
<section class="services">
  <div class="services__container container">
    <div class="services__content">
      <div class="services__header">
        <div class="services__section-name">услуги</div>
        <div class="services__title h3">
          <span>услуги нашей компании.</span>
        </div>
        <div class="services__desc">ежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный.</div>
        <div class="services__more">
          <a href="#">подробнее о нас</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="articles">
  <div class="articles__container container">
    <div class="articles__content">
      <article class="articles__article">
        <div class="articles__article-image" style="background-image: url('https://placeimg.com/640/480/people/sepia')"></div>
        <div class="articles__article-content">
          <div class="article__article-title">
            <small>блог</small>
            <h3>первая новость нашей компании.</h3>
          </div>
          <div class="articles__article-brief">
            <p>ежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный.
            </p>
            <div class="articles__article-more">читать дальше </div>
          </div>
        </div>
      </article>
      <article class="articles__article">
        <div class="articles__article-image" style="background-image: url('https://placeimg.com/640/480/people')"></div>
        <div class="articles__article-content">
          <div class="article__article-title">
            <small>блог</small>
            <h3>первая новость нашей компании.</h3>
          </div>
          <div class="articles__article-brief">
            <p>ежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный.
            </p>
            <div class="articles__article-more">читать дальше </div>
          </div>
        </div>
      </article>
    </div>
  </div>
</section>
<section class="feadbacks">
  <div class="feadbacks__container container">
    <div class="feadbacks__content">

      <div class="feadbacks__header">
        <div class="feadbacks__section-name">отзывы</div>
        <div class="feadbacks__title h3">
          <span>oтзывы наших клиентов.</span>
        </div>
      </div>
      <div class="feadbacks__slider">

      </div>
      <div class="feadbacks__desc">если Вы являетесь нашим клиентом и елаете поделится опытом сотрудничества с нами, оставьте свой отзыв</div>
      <div class="feadbacks__cta">
        <button class="button button--outline button--primary button--large">оставить отзыв</button>
      </div>

    </div>
  </div>
</section>
<section class="clients">
  <div class="clients__container container">
    <div class="clients__content">

      <div class="clients__header">
        <div class="clients__section-name">клиенты</div>
        <div class="clients__title h3">
          <span>с нами работают:</span>
        </div>
      </div>
      <div class="clients__list">

      </div>
      <div class="clients__cta">
        <button class="button button--outline button--primary button--large">подать заявку</button>
      </div>

    </div>
  </div>
</section>
<footer class="footer">
  <div class="footer__container container">
    <div class="footer__columns">
      <div class="footer__column footer__column--logo">

      </div>
      <div class="footer__column">
        <div class="footer__column-header h4">услуги.</div>
        <ul class="footer__column-list">
          <li class="footer__column-item">
            <a href="#">разработка сайтов</a>
          </li>
          <li class="footer__column-item">
            <a href="#">разработка сайтов</a>
          </li>
          <li class="footer__column-item">
            <a href="#">разработка сайтов</a>
          </li>
        </ul>
      </div>
      <div class="footer__column">
        <div class="footer__column-header h4">услуги.</div>
        <ul class="footer__column-list">
          <li class="footer__column-item">
            <a href="#">разработка сайтов</a>
          </li>
          <li class="footer__column-item">
            <a href="#">разработка сайтов</a>
          </li>
          <li class="footer__column-item">
            <a href="#">разработка сайтов</a>
          </li>
        </ul>
      </div>
      <div class="footer__column">
        <form class="footer__column-form">
          <input type="text" class="footer__column-input">
        </form>
        <div class="footer__column-socials">
          <div class="footer__column-social">
            <a href="#">
              <img src="@asset('images/fb.svg')" height="14" alt="">
            </a>
          </div>
          <div class="footer__column-social">
            <a href="#">
              <img src="@asset('images/tw.svg')" height="14" alt="">
            </a>
          </div>
          <div class="footer__column-social">
            <a href="#">
              <img src="@asset('images/in.svg')" height="14" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="footer__bottom">
      <div class="footer__copyright"></div>
      <nav class="footer__navigation">
        <ul class="footer__nav-list">
          <li class="footer__nav-item">
            <a href="#">главная</a>
          </li>
          <li class="footer__nav-item">
            <a href="#">услуги</a>
          </li>
          <li class="footer__nav-item">
            <a href="#">новости</a>
          </li>
          <li class="footer__nav-item">
            <a href="#">портфолио</a>
          </li>
          <li class="footer__nav-item">
            <a href="#">контакты</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

</footer>
