<header class="header">
  <div class="container header__container">
    <div class="header__logo">
      <a class="brand" href="{{ home_url('/') }}">
        <img src="@asset('images/SALogo.svg')" width="260" alt="SEO Assistant">
      </a>
    </div>
    <div class="header__content">
      <div class="header__contacts">
        <div class="header__call">
          <small>перезвонить?</small>
          <span>(050) 123 45 67</span>
        </div>
        <div class="header__socials">
          <div class="header__social">
            <a href="#">
              <img src="@asset('images/fb.svg')" height="14" alt="">
            </a>
          </div>
          <div class="header__social">
            <a href="#">
              <img src="@asset('images/tw.svg')" height="14" alt="">
            </a>
          </div>
          <div class="header__social">
            <a href="#">
              <img src="@asset('images/in.svg')" height="14" alt="">
            </a>
          </div>
          <div class="header__social">
            <a href="#">
              <img src="@asset('images/pin.svg')" height="14" alt="">
            </a>
          </div>
        </div>
      </div>
      <nav class="header__navigation">
        <div class="navigation__wrapper">
          @if (has_nav_menu('primary_navigation')) {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav'])
          !!} @endif
          <button class="header__contact-btn button button--primary">Заказать звонок</button>
        </div>
        <button class="hamburger hamburger--vortex" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      </nav>
    </div>
  </div>
</header>
