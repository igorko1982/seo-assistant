<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);


/**
 * Settings page
 */
add_action( 'admin_menu', 'sa__add_admin_menu' );
add_action( 'admin_init', 'sa__settings_init' );


function sa__add_admin_menu(  ) {

	add_options_page( 'Основные настройки', 'Основные настройки', 'manage_options', 'Основные_настройки', 'sa__options_page' );

}


function sa__settings_init(  ) {

	register_setting( 'options_page', 'sa__settings' );

	add_settings_section(
		'sa__options_page_section',
		__( 'Контактные данные', 'wordpress' ),
		'sa__settings_section_contacts',
		'options_page'
	);

	add_settings_field(
		'sa_phone_field',
		__( 'Телефон', 'wordpress' ),
		'sa_phone_field_render',
		'options_page',
		'sa__options_page_section'
	);

	add_settings_field(
		'sa_facebook_field',
		__( 'Ссылка на facebook', 'wordpress' ),
		'sa_facebook_field_render',
		'options_page',
		'sa__options_page_section'
	);

	add_settings_field(
		'sa_twitter_field',
		__( 'Ссылка на Twitter', 'wordpress' ),
		'sa_twitter_field_render',
		'options_page',
		'sa__options_page_section'
	);

	add_settings_field(
		'sa_linkedin_field',
		__( 'Ссылка на LinkedIn', 'wordpress' ),
		'sa_linkedin_field_render',
		'options_page',
		'sa__options_page_section'
	);

	add_settings_field(
		'sa_email_field',
		__( 'Email', 'wordpress' ),
		'sa_email_field_render',
		'options_page',
		'sa__options_page_section'
	);
}


function sa_phone_field_render(  ) {

	$options = get_option( 'sa__settings' );
	?>
	<input type='text' name='sa__settings[sa_phone_field]' value='<?php echo $options['sa_phone_field']; ?>'>
	<?php

}


function sa_facebook_field_render(  ) {

	$options = get_option( 'sa__settings' );
	?>
	<input type='text' name='sa__settings[sa_facebook_field]' value='<?php echo $options['sa_facebook_field']; ?>'>
	<?php

}


function sa_twitter_field_render(  ) {

	$options = get_option( 'sa__settings' );
	?>
	<input type='text' name='sa__settings[sa_twitter_field]' value='<?php echo $options['sa_twitter_field']; ?>'>
	<?php

}


function sa_linkedin_field_render(  ) {

	$options = get_option( 'sa__settings' );
	?>
	<input type='text' name='sa__settings[sa_linkedin_field]' value='<?php echo $options['sa_linkedin_field']; ?>'>
	<?php

}


function sa_email_field_render(  ) {

	$options = get_option( 'sa__settings' );
	?>
	<input type='text' name='sa__settings[sa_email_field]' value='<?php echo $options['sa_email_field']; ?>'>
	<?php

}


function sa__settings_section_contacts(  ) {

	echo __( 'Контактные данные из этой страницы будут использована в разных местах на сайте.', 'wordpress' );

}


function sa__options_page(  ) {

	?>
	<form action='options.php' method='post'>

		<h1>Основные настройки сайта</h1>

		<?php
		settings_fields( 'options_page' );
		do_settings_sections( 'options_page' );
		submit_button();
		?>

	</form>
	<?php

}

// Register Custom Post Type
function post_type_slider() {

	$labels = array(
		'name'                  => _x( 'Слайды', 'Слайды для слайдера', 'wordpress' ),
		'singular_name'         => _x( 'Слайд', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'             => __( 'Слайды', 'wordpress' ),
		'name_admin_bar'        => __( 'Слайд', 'wordpress' ),
		'archives'              => __( 'Item Archives', 'wordpress' ),
		'attributes'            => __( 'Item Attributes', 'wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'wordpress' ),
		'all_items'             => __( 'Все слайды', 'wordpress' ),
		'add_new_item'          => __( 'Добавить слайд', 'wordpress' ),
		'add_new'               => __( 'Добавить новый', 'wordpress' ),
		'new_item'              => __( 'Новый слайд', 'wordpress' ),
		'edit_item'             => __( 'Редактировать', 'wordpress' ),
		'update_item'           => __( 'Обновить', 'wordpress' ),
		'view_item'             => __( 'Посмотреть слайд', 'wordpress' ),
		'view_items'            => __( 'Посмотреть слайды', 'wordpress' ),
		'search_items'          => __( 'Поиск', 'wordpress' ),
		'not_found'             => __( 'Не найдено', 'wordpress' ),
		'not_found_in_trash'    => __( 'Не найдено в корзине', 'wordpress' ),
		'featured_image'        => __( 'Изображение', 'wordpress' ),
		'set_featured_image'    => __( 'Выбрать изобраденеи', 'wordpress' ),
		'remove_featured_image' => __( 'Удалить изображенеи', 'wordpress' ),
		'use_featured_image'    => __( 'Использовать как основное изображение', 'wordpress' ),
		'insert_into_item'      => __( 'Вставить', 'wordpress' ),
		'uploaded_to_this_item' => __( 'Загрузить', 'wordpress' ),
		'items_list'            => __( 'Список', 'wordpress' ),
		'items_list_navigation' => __( 'Навигация', 'wordpress' ),
		'filter_items_list'     => __( 'Фильтр', 'wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Слайды', 'wordpress' ),
		'description'           => __( 'Слайды для слайдера', 'wordpress' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'slider', $args );

}
add_action( 'init', 'post_type_slider', 0 );

