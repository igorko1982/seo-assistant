import { tns } from 'tiny-slider'

export default {
  init() {
    tns({
      container: '.intro__slider .slider__slide',
      items: 1,
      slideBy: 'page',
      autoplay: false,
    })
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
}
