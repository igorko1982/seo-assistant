import SVGInjector from 'svg-injector'

export default {
  init() {
    const svgsToInject = document.querySelectorAll('img.inject')

    SVGInjector(svgsToInject)
    $('.hamburger').click(function() {
      $(this).toggleClass('is-active')
    })
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
}
