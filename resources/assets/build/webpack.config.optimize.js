'use strict' // eslint-disable-line

const glob = require('glob-all')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const { default: ImageminPlugin } = require('imagemin-webpack-plugin')
const imageminMozjpeg = require('imagemin-mozjpeg')

const config = require('./config')

module.exports = {
  plugins: [
    new ImageminPlugin({
      optipng: { optimizationLevel: 7 },
      gifsicle: { optimizationLevel: 3 },
      pngquant: { quality: '65-90', speed: 4 },
      svgo: { removeUnknownsAndDefaults: false, cleanupIDs: false },
      plugins: [imageminMozjpeg({ quality: 75 })],
      disable: config.enabled.watcher,
    }),
    new PurgecssPlugin({
      paths: glob.sync([
        'app/**/*.php',
        'resources/views/**/*.php',
        'resources/assets/scripts/**/*.js',
      ]),
    }),
  ],
}
